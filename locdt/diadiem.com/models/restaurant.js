"use strict";

module.exports = function(sequelize, DataTypes) {
    var Restauant = sequelize.define("restaurant", {
        id: { 
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {type: DataTypes.STRING},
        address: {type: DataTypes.STRING},
        city: {type: DataTypes.STRING},
        phone: {type: DataTypes.STRING},
        rating: {type: DataTypes.INTEGER},
        x: {type: DataTypes.DECIMAL},
        y: {type: DataTypes.DECIMAL},
        slogan: {type: DataTypes.STRING},
        email: {type: DataTypes.STRING},
        website: {type: DataTypes.STRING}
        // note: { type: DataTypes.TEXT },
        // textJson: { type: DataTypes.TEXT },
        // cityId: { type: DataTypes.INTEGER }

    }, {
        tableName: "restaurant"
    }
    , {
        classMethods: {
        }
    });

    return Restauant;
};