/**
 * Created by dinht_000 on 4/28/2017.
 */
'use strict';

var restaurant = require('./dbservices/restaurant.js');
var request = require('request');
var cities = require('./config/city.json');

var HOST = "http://www.diadiem.com/";

function getRestaurantJsonData(location, page) {
    console.log("Start get data in: " + location + " at page: " + page);
    let headers = {
        'User-Agent':       'Super Agent/0.0.1',
        'Content-Type':     'application/x-www-form-urlencoded'
    };

    var options = {
        url: 'http://www.diadiem.com/Comp.aspx',
        method: 'POST',
        headers: headers,
        form: {
            "stpe": 1,
            "st":   location,
            "F":    "",
            "C":    "",
            "K":    "Nha hang",
            "A":    "0_0",
            "S":    "M",
            "P":    page,
            "R":    40,
            "fD":   1,
            "uID":  0,
            "cID":  0,
            "dID":  0
        }
    };

    return new Promise((resolve, reject) => {
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                resolve(body);
            }
        });
    });
}
function parseData(data, city) {
    data.dsCompany.forEach((element) => {
        let TCty = element.TCty;
        if (TCty.includes("Nhà Hàng") || TCty.includes("Restaurant") || TCty.includes("restaurant")) {
            let Restaurant = {
                name: element.TCty,
                address: element.Add,
                city: city,
                phone: element.P,
                rating: parseInt(element.Sta),
                x: parseFloat(element.X),
                y: parseFloat(element.Y),
                slogan: element.Slogan,
                email: element.Email,
                website: element.Link
            };
            restaurant.add(Restaurant);
        }
    });
}
function requestData(index, page) {
    let cityCode = cities[index].code;
    let cityName = cities[index].name;
    getRestaurantJsonData(cityCode, page).then(data => {
        data = data.toString().replace(/\\"/g, "'").replace(/\\/g, "");
        let json = JSON.parse(data);

        console.log("Getting data...");
        if (json.dsCompany.length == 0)
            requestData(++index, 1);
        else {
            parseData(json, cityName);
            requestData(index, ++page);
        }
    });
};

// main
requestData(0, 1);
