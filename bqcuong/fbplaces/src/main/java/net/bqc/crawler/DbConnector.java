package net.bqc.crawler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {

    private static DbConnector instance = new DbConnector();

    private DbConnector() {
    }

    public static DbConnector getInstance() {
        return instance;
    }

    public Connection createConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/*?useSSL=false",
                    "*", "*");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return con;
        }
    }
}


