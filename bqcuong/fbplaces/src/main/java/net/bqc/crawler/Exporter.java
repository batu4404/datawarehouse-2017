package net.bqc.crawler;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cuong on 4/30/2017.
 */
public class Exporter {

    public static void main(String[] args) throws SQLException {
        List<Restaurant> restaurants = collectFromDatabase();
        exportToExcel(restaurants);

    }

    public static List<Restaurant> collectFromDatabase() throws SQLException {
        List<Restaurant> restaurants = new ArrayList<>();
        Connection connection = DbConnector.getInstance().createConnection();

        final String SELECT_QUERY = "SELECT `name`,`city`,`street`,`lat`,`lon`,`id` FROM `fbplace`";
        PreparedStatement statement = connection.prepareStatement(SELECT_QUERY);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            Restaurant restaurant = new Restaurant();
            restaurant.setFbid(rs.getString(6));
            restaurant.setName(rs.getString(1));
            restaurant.setCity(rs.getString(2));
            restaurant.setAddress(rs.getString(3));
            restaurant.setLat(rs.getString(4));
            restaurant.setLon(rs.getString(5));
            restaurants.add(restaurant);
        }
        connection.close();
        return restaurants;
    }

    public static void exportToExcel(List<Restaurant> restaurants) {
        try (InputStream templateIs = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("fb_res_template.xlsx")) {
            try (OutputStream os = new FileOutputStream("fb_restaurants.xlsx")) {
                Context context = new Context();
                context.putVar("restaurants", restaurants);
                JxlsHelper.getInstance().processTemplate(templateIs, os, context);
                os.close();
            }
            templateIs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
