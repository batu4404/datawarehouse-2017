package net.bqc.crawler;

import com.restfb.*;
import com.restfb.types.Location;
import com.restfb.types.Place;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * Hello world!
 */
public class App {

    static final String FB_ACCESS_TOKEN = "*";

    static final String INSERT_PLACE_QUERY = "insert into `fbplace` (`id`, `name`, `city`, `street`, `lon`, `lat`) " +
            "VALUES (?, ?, ?, ?, ?, ?)";

    public static void main(String[] args) throws SQLException {
        // Hanoi Post Office: 21.0283086,105.8198198
        // Nghe An PO: 18.6706041,105.682003
        // Da Nang PO: 16.0462599,108.133914
        // HCM City PO : 10.8241301,106.6822298
        // Phan Thiet PO: 10.9248636,108.0926622
        // Quang Ninh PO: 20.9520116,107.0835417
        // Lam Dong PO: 11.9465062,108.459091

        FacebookClient facebookClient = new DefaultFacebookClient(FB_ACCESS_TOKEN, Version.VERSION_2_8);
        Connection<Place> publicPlaces = facebookClient.fetchConnection("search", Place.class,
                Parameter.with("q", "restaurant"),
                Parameter.with("center", "10.8241301,106.6822298"),
                Parameter.with("distance", 100000), // 100km
                Parameter.with("fields", "id"),
                Parameter.with("type", "place"));

        java.sql.Connection dbConnection = DbConnector.getInstance().createConnection();
        int count = 0;

        for (List<Place> places : publicPlaces) {
            for (Place place : places) {
                Place detailedPlace = facebookClient.fetchObject(place.getId(), Place.class,
                        Parameter.with("fields", "name,category_list,location"));

                String placeId = place.getId();
                String placeName = detailedPlace.getName();
                String city = null, lon = null, lat = null, street = null;
                Location location = detailedPlace.getLocation();
                if (location != null) {
                    city = location.getCity();
                    lon = Double.toString(location.getLongitude());
                    lat = Double.toString(location.getLatitude());
                    street = location.getStreet();
                }

                Restaurant restaurant = new Restaurant();
                restaurant.setFbid(placeId);
                restaurant.setName(placeName);
                restaurant.setCity(city);
                restaurant.setAddress(street);
                restaurant.setLat(lat);
                restaurant.setLon(lon);

                System.out.print(String.format("[%d] ", ++count));
                System.out.println(restaurant);

                try {
                    PreparedStatement preparedStatement = dbConnection.prepareStatement(INSERT_PLACE_QUERY);
                    preparedStatement.setString(1, placeId);
                    preparedStatement.setString(2, placeName);
                    preparedStatement.setString(3, city);
                    preparedStatement.setString(4, street);
                    preparedStatement.setString(5, lon);
                    preparedStatement.setString(6, lat);

                    preparedStatement.executeUpdate();
                    System.out.println("Inserted!\n");
                } catch (SQLIntegrityConstraintViolationException e) {
                    System.err.println(String.format("[%d]: %s", count, e.getMessage()));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Done!\n");
        dbConnection.close();
    }

}
