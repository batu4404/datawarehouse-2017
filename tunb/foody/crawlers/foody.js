var models = require('../models');
var request = require('request');
var cheerio = require('cheerio');
var events = require('events');
var fs = require('fs');

var logStream = fs.createWriteStream('./log.txt', {'flags': 'a'});

// Tao mot doi tuong eventEmitter
var eventEmitter = new events.EventEmitter();

var otherCityId = 219;

var countItem = 0;
var lastestId;

var options = {
    url: 'https://www.foody.vn/__get/Place/HomeListPlace',
    headers: {
        'Accept':'application/json, text/javascript',
        'Origin':'www.foody.vn',
        'Host': 'www.foody.vn',
        'X-Requested-With':'XMLHttpRequest',
        'X-Foody-User-Token':'t3u2D3nLWWRydj3IFYEY6B3va05JxSgcdwKUWr8zXNL2ZcamWVkGjBhL3sDp',
        'User-Agent':'Mozilla/.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
        'Cookie':'flg=vn; floc=218; gcat=food; _ga=GA1.2.1122353276.1492610098; FOODY.AUTH.UDID=b4e89e87-3280-4333-b207-60117b03dcb2; FOODY.AUTH=E25FD26EBF1BD4BC6DE92BA52C2D1ECE9A1E2A18FB2733FD9DA84B81E4CC523457F3107C8B3F0D5C1E2B5BBA82CDF9DD6A3E05129F514020B4542228634671D70B214F701F61F15474BF438BD069AD6635B37C19B061C7BE3C046A7296804FA652C6CAC4A1590D9644EA58695BA8D8C1207AEDD25AD9D16E4C9849C3B3E510DFEA6ADB78B84B73210854707771E8EAD6F41EBA991C8E87394A8409A938E912CC5205BF74A6A50C02DDD3AB0B4FCD3F660ECA58C477FA316FE8C74099D8D6C679395710188231C74A97A746A914B75C0587291B07E6BCEB5749D9F3353691E503; _gat_ads=1'
    },
    qs: {
        't':'1492610120958',
        'page':'1',
        'lat':'21.033333',
        'lon':'105.85',
        'count':'20',
        'lastId':'',
        'ExcludeIds':',,193590,169399',
        'districtId':'',
        'cityId':'218',
        'cateId':'',
        'cuisineId':'',
        'isReputation':'',
        'type':'1'
    },
};

//'lastId':'39646',

// options.url = 'https://www.foody.vn/__get/Place/HomeListPlace?t=1492424998363&page=1&count=500&lastId=134285&ExcludeIds=,,193590,169399&districtId=&cityId=218&cateId=&cuisineId=&isReputation=&type=1';

//options.url = 'https://www.foody.vn/__get/Place/HomeListPlace?t=1492424998363&page=1&count=500&lastId=&ExcludeIds=,,193590,169399&districtId=&cityId=218&cateId=&cuisineId=&isReputation=&type=1';

function addItem(item, cityId) {
    var id = item.Id;
    var name = item.Name;
    var address = item.Address;
    var rating = item.AvgRatingText;
    var lat = item.Latitude;
    var long = item.Longitude;
    var phone = item.Phone;

    models.restaurant_foody.create({
        id: id,
        name: name,
        address: address,
        rating: rating,
        lat: lat,
        long: long,
        phone: phone,
        cityId: cityId,
        textJson: JSON.stringify(item)
    }).then(function(item) {
//        console.log('xong mot chu');
        countItem++;
    }).catch(function(err) {
//        console.log('error when insert item: ' + err);
    });
}

module.exports.test = function() {
    request(options, function(err, res, data) {
        console.log("err: " + err);
        if (err) throw err;
        console.log("get done!");
    //    console.log(data);

        var jsonObject = JSON.parse(data);
    //    var jsonObject = data;  

    //   console.log(jsonObject);
        console.log('items: ' + jsonObject.Items.length);

    //    countItem += sonObject.Items.length;

        items.forEach(function(item) {
            addItem(item);  
        });
    }); 
}

function getItems(cityId, count, page, lastId) {

    options.qs.cityId = cityId;
    options.qs.count = count;
    options.qs.page = page;
    options.qs.lastId = lastId;

    request(options, function(err, res, data) {
        console.log("err: " + err);
        if (err) throw err;
        console.log("get done!");
    //    console.log(data);

        var jsonObject = JSON.parse(data);
    //    var jsonObject = data;  

    //   console.log(jsonObject);
        console.log('items: ' + jsonObject.Items.length);

        if (jsonObject.Items.length < 1) {
            eventEmitter.emit('xong_mot_chau');
            return;
        }

        var items = jsonObject.Items;

        items.forEach(function(item) {
            addItem(item, cityId);  
        });

        console.log(countItem + ' chu da duoc them vao csdl');
        console.log('page: %s ; count: %s ; lastId: %s', page, count, lastId);

        lastId = jsonObject.LastId;

        if (lastId === lastestId) {
            console.log('lastest Id');
            console.log('xong id: ' + cityId);
            logStream.write('xong id: ' + cityId + '\r\n');
            logStream.write(countItem + ' chu da duoc them vao csdl' + '\r\n');
            logStream.write('================================================' + '\r\n');
            eventEmitter.emit('xong_mot_chau');
            return;
        }
        else {
            lastestId = lastId;
        }

        var totalItems = jsonObject.Total;
        if (countItem >= totalItems) {
            eventEmitter.emit('xong_mot_chau');
            return;
        }

        setTimeout(function() {
            getItems(cityId, count, page+1, lastestId)
        }, 5000);
//        getItems(cityId, count, page++, lastId)
    }); 
}

module.exports.getData = function() {
    var lastId = 124716;
    getItems(217, 70, 1, 84978);
//    getItems(217, 100, 1, 84978);
//    getItems(218, 500, 1);
}

module.exports.getDataOtherCity = function() {
    otherCityId = 226;

    console.log("id: " + otherCityId);
    logStream.write('bat dau: ' + otherCityId + '\r\n');
    getItems(otherCityId, 100, 1);
}


// Gan ket event voi Event Handler nhu sau:
eventEmitter.on('xong_mot_chau', function() {
    if (otherCityId > 279) {
        console.log("xong het cac tinh khac !!!!!!!!!!!!!!!!!!!");
        logStream.write("xong het cac tinh khac !!!!!!!!!!!!!!!!!!!\r\n");
        return;
    }

    otherCityId++;
    countItem = 0;
    console.log("bat dau: " + otherCityId);
    logStream.write('bat dau: ' + otherCityId + '\r\n');
    getItems(otherCityId, 100, 1)
}); 