"use strict";

module.exports = function(sequelize, DataTypes) {
    var RestauantFoody = sequelize.define("restaurant_foody", {
        // id de autoIncrement thi khi create data se co chua truong id
        id: { 
            type: DataTypes.INTEGER, 
            primaryKey: true
        },
        name: { type: DataTypes.STRING },
        address: { type: DataTypes.STRING },
        lat: { type: DataTypes.DECIMAL },
        long: { type: DataTypes.DECIMAL },
        phone: { type: DataTypes.STRING},
        rating: { type: DataTypes.STRING },
        note: { type: DataTypes.TEXT },
        textJson: { type: DataTypes.TEXT },
        cityId: { type: DataTypes.INTEGER }

    }, {
        tableName: "restaurant_foody"
    }
    , {
        classMethods: {
        }
    });

    return RestauantFoody;
};