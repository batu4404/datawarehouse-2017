package getData;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class amthuc365 {
	static long soluongbanghi = 0;
	public static Connection getMyConnection() throws SQLException, ClassNotFoundException {
		// Sử dụng Oracle.
		// Bạn có thể thay thế bởi Database nào đó.
		return MySQLConnUtils.getMySQLConnection();
	}
	
	
	public static void main(String args[]) throws IOException, ClassNotFoundException, SQLException{
		long start, end;
		start = System.nanoTime();
		String city[] = {"ha-noi", 
				"tp-ho-chi-minh", 
				"an-giang", 
				"bac-kan", 
				"bac-giang", 
				"bac-lieu", 
				"bac-ninh", 
				"ben-tre",
				"khanh-hoa", 
				"binh-dinh", 
				"binh-duong", 
				"binh-phuoc", 
				"binh-thuan", 
				"ca-mau",
				"can-tho",
				"cao-bang",
				"da-nang",
				"lai-chau",
				"dong-nai",
				"dong-thap",
				"gia-lai",
				"ha-giang",
				"ha-nam",
				"ha-tinh",
				"hai-duong",
				"hai-phong",
				"hoa-binh",
				"thua-thien-hue",
				"hung-yen",
				"khanh-hoa",
				"kon-tum",
				"dien-bien",
				"lam-dong",
				"lang-son",
				"lao-cai",
				"long-an",
				"nam-dinh",
				"nghe-an",
				"ninh-binh",
				"ninh-thuan",
				"phu-tho",
				"phu-yen",
				"quang-binh",
				"quang-nam",
				"quang-ngai",
				"quang-ninh",
				"quang-tri",
				"soc-trang",
				"son-la",
				"tay-ninh",
				"thai-binh",
				"thai-nguyen",
				"thanh-hoa",
				"tien-giang",
				"tra-vinh",
				"tuyen-quang",
				"kien-giang",
				"vinh-long",
				"vinh-phuc",
				"ba-riavung-tau",
				"hau-giang",
				"yen-bai",
				"dak-lak",
				"dak-nong"};
		for(int i = 0; i < 64; i++){
				String url = "http://www.amthuc365.vn/" + city[i] + "/";
				System.out.println(url);
				Document document = Jsoup.connect(url).get();
				int count = 0;
				if(document.select(".pagination .page").last() == null)
					count = 0;
				else count = Integer.parseInt(document.select(".pagination .page").last().text());
				//System.out.println(count);
				
				Connection connection = ConnectionUtils.getMyConnection();
				int j = 1;
				for(; j <= count; j++){
					System.out.println(city[i] + ": " + j);
					Document doc = Jsoup.connect(url+"trang-"+j+"/").get();
					
					Elements list = doc.select(".restaurant_search_list .restaurant_el");
					for(Element alist : list){
						String name = alist.select("h3").text().replaceAll("\'", "\\\\'").replaceAll("\"", "\\\\'");
						String address = alist.select("address").text().replaceAll("Xem bản đồ", "").replaceAll("\"", "\\\\'");
						String line = alist.select(".line").text();
					
						String des = alist.select(".highlight").text().replaceAll("\'", "\\\\'").replaceAll("\"", "\\\\'");
						//System.out.println(des);
						int rate = Integer.parseInt(alist.select("span").text());
						String url2 = "http://www.amthuc365.vn" + alist.select("h3 a").attr("href");
						Document doc2;
						try{
							doc2 = Jsoup.connect(url2).get();
						}catch(HttpStatusException e){
							break;
						}
						Elements data = doc2.select("div.line.wrap");
						if(data.size() < 3)
							break;
						String fanpage = data.get(1).text();
						if(!fanpage.contains("http")){
							fanpage = "http://www.amthuc365.vn" + fanpage;
						}
						String time = data.get(2).text();
						String price = data.get(3).text();
						//System.out.println(fanpage + " + " + time + " + " + price);
						Statement statement = connection.createStatement();
						 
					    String sql = "Insert into restaurant(name, address, line, price, rate, website, description, time) "
					              + " values (" + "\'"+name +"\'" + ", " + "\"" + address + "\"" + ", " + "\"" + line +"\""+ ", " + "\"" + price + "\"" + ", " + rate + ", " + "\""+ fanpage+"\"" + ", " + "\'"+ des +"\'"+ ", " + "\""+ time +"\"" + ") ";
					    
						//String sql ="Insert into restaurant(name, address, price, rate, time, phone, website, description)  values (Nhà hàng Á Gia, 36 Quang Trung - Quận Hoàn Kiếm - Hà Nội , N/A, 3, 0 - 0, 04 39426565, http:// www.agia.vn/., Thực khách có thể thưởng thức hương vị ẩm thực đường phố 6 nước châu Á đặc sắc với menu buffet hơn 100 món ăn được phục vụ, trong không gian thanh lịch cùng dịch vụ chuyên nghiệp.)";
						
						System.out.println(sql);
					    
					    // Thực thi câu lệnh.
					      // executeUpdate(String) sử dụng cho các loại lệnh Insert,Update,Delete.
					    statement.executeUpdate(sql);
					    soluongbanghi++;
					}
					System.out.println("So luong ban ghi :" + soluongbanghi);
				}
				
		}
		end = System.nanoTime();
		System.out.println("So luong ban ghi: "+soluongbanghi );
		System.out.println("Thoi gian thuc hien: " + (end - start));
		
		
	}
}
